﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Musikant.Data;
using System.IO;
using Musikant.Utils;
using System.Windows.Forms;

namespace Musikant.Facade
{
    class MusikantFacade
    {
        MusikantDataSet dataSet;
        DirectoryInfo projectsDir;
        Dictionary<long, DirectoryInfo> projectsDict;
        List<long> newProjectNumbers;
        List<string> dawExtensions_ = new List<string>(new string[] { ".song", ".reason", ".als", ".rpp", ".xrns", ".flp", ".cpr" });
        Dictionary<string, string> daws = new Dictionary<string, string>()
        {
            { ".song", "Studio One" },
            { ".reason", "Reason"  },
            { ".als", "Ableton Live" },
            { ".rpp", "Reaper" },
            { ".xrns", "Renoise" },
            { ".flp", "FL Studio" },
            { ".cpr", "Cubase" }
        };
        List<string> sounds = new List<string>()
        {
            ".wav",
            ".mp3",
            ".mid"
        };

        public MusikantFacade(MusikantDataSet dataSet)
        {
            this.dataSet = dataSet;
            Initialize();
        }

        public void Initialize()
        {
            projectsDir = new DirectoryInfo(dataSet.GetConfigValue(Constants.CONFIG_PROJECT_DIR));
            ScanProjectDirectory();
            UpdateDataSet();
            SortDataSet();
        }

        private void ScanProjectDirectory()
        {
            projectsDict = new Dictionary<long, DirectoryInfo>();
            newProjectNumbers = new List<long>();

            DirectoryInfo[] dir1 = projectsDir.GetDirectories();
            foreach (DirectoryInfo subDir in dir1)
            {
                string subDirName = subDir.Name;
                long projNum = StringUtils.StartingNumber(subDir.Name);
                if (projNum > 0)
                {
                    projectsDict.Add(projNum, subDir);
                    MusikantDataSet.ProjectRow dr = dataSet.Project.AsEnumerable().SingleOrDefault(r=> r.ProjectID == projNum);
                    if (dr == null)
                    {
                        var created = DateTime.MaxValue;
                        var modified = DateTime.MinValue;
                        var rewire = false;
                        string daw = Constants.UNKNOWN;
                        foreach (FileInfo file in subDir.GetFiles())
                        {
                            if (daws.ContainsKey(file.Extension.ToLower()))
                            {
                                if (file.LastWriteTime.CompareTo(modified) > 0) modified = file.LastWriteTime;
                                if (file.CreationTime.CompareTo(created) < 0) created = file.CreationTime;
                                var ext = file.Extension.ToLower();
                                if ((ext == Constants.EXT_REASON && daw != Constants.UNKNOWN && daw != Constants.DAW_REASON)
                                    || (ext != Constants.EXT_REASON && daw == Constants.DAW_REASON))
                                {
                                    rewire = true;
                                }
                                if (!(ext == Constants.EXT_REASON && daw != Constants.UNKNOWN))
                                {
                                    daw = daws[file.Extension.ToLower()];
                                }
                            }
                        }
                        newProjectNumbers.Add(projNum);
                        MusikantDataSet.ProjectRow newRow = dataSet.Project.NewProjectRow();
                        newRow.ProjectID = projNum;
                        newRow.Name = subDir.Name;
                        newRow.Created = created;
                        newRow.Modified = modified;
                        newRow.DAW = daw;
                        newRow.Genre = Constants.UNKNOWN;
                        newRow.Status = 1;
                        newRow.Potential = 1;
                        newRow.Rewire = rewire.ToString();
                        newRow.Tempo = 30; // unterste Grenze
                        dataSet.Project.AddProjectRow(newRow);
                    }
                }
                                
            }
        }

        private void UpdateDataSet()
        {
            for (int i = dataSet.Project.Rows.Count - 1; i >= 0; i--) // In foreach kann man nicht löschen
            {
                MusikantDataSet.ProjectRow curRow = (MusikantDataSet.ProjectRow)dataSet.Project.Rows[i];
                if (IsCurrentProject(curRow.ProjectID))
                {
                    
                    DirectoryInfo curDir = projectsDict[curRow.ProjectID];
                    if (curRow.Name != curDir.Name)
                    {
                        curRow.Name = curDir.Name;
                    }
                    foreach (FileInfo file in curDir.GetFiles())
                    {
                        if (daws.ContainsKey(file.Extension.ToLower()) && file.LastWriteTime.CompareTo(curRow.Modified) > 0)
                        {
                            curRow.Modified = file.LastWriteTime;
                        }
                    }
                }
                else
                {
                    dataSet.Project.Rows[i].Delete();
                }
            }
        }

        private void SortDataSet()
        {
            dataSet.Project.DefaultView.Sort = "ProjectID DESC";
        }

        public DirectoryInfo CurrentDirectory(MusikantDataSet.ProjectRow curProject)
        {
            string curDir = string.Format(@"{0}\{1}", projectsDir.FullName, curProject.Name);
            return new DirectoryInfo(curDir);
        }

        public void ListDirectory(TreeView treeView, MusikantDataSet.ProjectRow curProject)
        {
            treeView.Nodes.Clear();

            var stack = new Stack<TreeNode>();
            var rootDirectory = CurrentDirectory(curProject);
            var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
            node.ImageKey = "Folder";
            node.SelectedImageKey = node.ImageKey;
            stack.Push(node);

            while (stack.Count > 0)
            {
                var currentNode = stack.Pop();
                var directoryInfo = (DirectoryInfo)currentNode.Tag;
                foreach (var directory in directoryInfo.GetDirectories())
                {
                    if (directory.Name.ToLower() == "history")
                    {
                        DeleteOldHistory(directory);
                    }
                    var childDirectoryNode = new TreeNode(StringUtils.Truncate(directory.Name)) { Tag = directory };
                    childDirectoryNode.ImageKey = "Folder";
                    childDirectoryNode.SelectedImageKey = childDirectoryNode.ImageKey;
                    currentNode.Nodes.Add(childDirectoryNode);
                    stack.Push(childDirectoryNode);
                }
                foreach (var file in directoryInfo.GetFiles())
                {
                    TreeNode newNode = new TreeNode(StringUtils.Truncate(file.Name));
                    newNode.Tag = file.FullName;
                    if (daws.ContainsKey(file.Extension.ToLower())) 
                    {
                        newNode.ImageKey = daws[file.Extension.ToLower()];
                    } 
                    else if (sounds.Contains(file.Extension.ToLower())) 
                    {
                        newNode.ImageKey = "Sound";
                    }
                    else 
                    {
                        newNode.ImageKey = "File";
                    }
                    newNode.SelectedImageKey = newNode.ImageKey;
                    currentNode.Nodes.Add(newNode);
                }
            }

            treeView.Nodes.Add(node);
            foreach (TreeNode tn in treeView.Nodes)
            {
                tn.Expand(); // nur erste Ebene aufklappen
            }
        }

        /**
         * Lösche alte StudioOne History-Files, damit Folder nicht zu groß wird.
         * Es sollen nur max 10 drinliegen
         */
        private void DeleteOldHistory(DirectoryInfo directory)
        {
            var sortedFiles = directory.GetFiles("*.song").OrderByDescending(f => f.LastWriteTime).ToList();
            for (int i=0; i<sortedFiles.Count; i++)
            {
                if (i > 10)
                {
                    sortedFiles[i].Delete();
                }
            }
        }

        public void RenameProject(MusikantDataSet.ProjectRow curProject, string newName)
        {
            var rootDirectory = CurrentDirectory(curProject);
            string oldName = StringUtils.RemoveStartingNumber(curProject.Name);

            List<FileInfo> list = projectsDict[curProject.ProjectID].GetFiles("*.*", SearchOption.AllDirectories).ToList();
            list = list.Where(s => daws.ContainsKey(Path.GetExtension(s.FullName))).ToList();
            foreach (FileInfo dawFile in list)
            {
                if (dawFile.Name.ToLower().Contains(oldName.ToLower()))
                {
                    string newFileName = dawFile.Name.ToLower().Replace(oldName.ToLower(), newName);
                    //Console.WriteLine(newFileName);
                    //Console.WriteLine(Path.Combine(dawFile.DirectoryName, newFileName));
                    File.Move(dawFile.FullName, Path.Combine(dawFile.DirectoryName, newFileName));
                }
            }
            list = null;
            string newProjectName = StringUtils.GetStartingNumber(curProject.Name) + " " + newName;
            string newDirName = string.Format(@"{0}\{1}", projectsDir.FullName, newProjectName);
            //Console.WriteLine(newDirName);
            rootDirectory.MoveTo(newDirName);
            curProject.Name = newProjectName;
            projectsDict[curProject.ProjectID] = new DirectoryInfo(newDirName);
        }

        public bool IsNewProject(long projectID)
        {
            return newProjectNumbers.Contains(projectID);
        }
        public bool IsCurrentProject(long projectID)
        {
            return projectsDict.ContainsKey(projectID);
        }

        public FileInfo FindFinalOrDemo(long projectID) {
            List<FileInfo> list = projectsDict[projectID].GetFiles("*.*", SearchOption.AllDirectories).ToList();
            list = list.Where(s => sounds.Contains(Path.GetExtension(s.FullName))).ToList();
            FileInfo useThisFile = null;
            foreach (FileInfo soundFile in list) {
                if (soundFile.Name.ToLower().Contains("final"))
                {
                    if (useThisFile == null 
                        || useThisFile.Name.ToLower().Contains("demo")
                        || soundFile.LastWriteTime.CompareTo(useThisFile.LastWriteTime) > 0)
                    {
                        useThisFile = soundFile;
                    }
                } else if (soundFile.Name.ToLower().Contains("demo"))
                {
                    if (useThisFile == null
                        || ( ! useThisFile.Name.ToLower().Contains("final") && soundFile.LastWriteTime.CompareTo(useThisFile.LastWriteTime) > 0))
                    {
                        useThisFile = soundFile;
                    }
                }
            }

            return useThisFile;
        }

        public long DirectorySize(DirectoryInfo d)
        {    
            long size = 0;    
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis) 
            {      
                size += fi.Length;    
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis) 
            {
                size += DirectorySize(di);   
            }
            return size;  
        }

        public int NumberOfFiles(DirectoryInfo d)
        {
            int files = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                files++;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                files += NumberOfFiles(di);
            }
            return files;
        }

        public bool IsSoundFile(FileInfo fileInfo)
        {
            return sounds.Contains(fileInfo.Extension);
        }

        public void CopyDirectory(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);
            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                Console.WriteLine(@"Copying {0}\{1}", target.FullName, fi.Name);
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }
            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyDirectory(diSourceSubDir, nextTargetSubDir);
            }
        }
    }
}
