﻿namespace Musikant
{
    partial class MusikantForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusikantForm));
            this.musikantDataGridView = new System.Windows.Forms.DataGridView();
            this.ProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameText = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Created = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DAW = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rewire = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Potential = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ideas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Todos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Genre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tempo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gridContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.masteringMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteProjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameProjectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.musikantDataSet = new Musikant.Data.MusikantDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rewireCheckBox = new System.Windows.Forms.CheckBox();
            this.btnMemory2 = new System.Windows.Forms.Button();
            this.btnMemory4 = new System.Windows.Forms.Button();
            this.btnMemory3 = new System.Windows.Forms.Button();
            this.btnMemory1 = new System.Windows.Forms.Button();
            this.btnSet4 = new System.Windows.Forms.Button();
            this.btnSet3 = new System.Windows.Forms.Button();
            this.btnSet2 = new System.Windows.Forms.Button();
            this.btnSet1 = new System.Windows.Forms.Button();
            this.resetButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.potentialComboBox = new System.Windows.Forms.ComboBox();
            this.tempoComboBox = new System.Windows.Forms.ComboBox();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.statusComboBox = new System.Windows.Forms.ComboBox();
            this.genreComboBox = new System.Windows.Forms.ComboBox();
            this.filterTextBox = new System.Windows.Forms.TextBox();
            this.modifiedComboBox = new System.Windows.Forms.ComboBox();
            this.dawComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupsListBox = new System.Windows.Forms.CheckedListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.tempo4HzTextBox = new System.Windows.Forms.Label();
            this.tempo16HzTextBox = new System.Windows.Forms.Label();
            this.tempo4MsTextBox = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tempo8MsTextBox = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tempo1MsTextBox = new System.Windows.Forms.Label();
            this.tempo4dMsTextBox = new System.Windows.Forms.Label();
            this.tempo16MsTextBox = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.editTempoUpDown = new System.Windows.Forms.NumericUpDown();
            this.editRewireCheckBox = new System.Windows.Forms.CheckBox();
            this.todoTextBox = new System.Windows.Forms.TextBox();
            this.infosTextBox = new System.Windows.Forms.TextBox();
            this.ideasTextBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.filesTreeView = new System.Windows.Forms.TreeView();
            this.musikantImageList = new System.Windows.Forms.ImageList(this.components);
            this.fileInfoLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.editDawComboBox = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.editGenreComboBox = new System.Windows.Forms.ComboBox();
            this.editStatusComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.editPotentialComboBox = new System.Windows.Forms.ComboBox();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rescanMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.newProjNumLabel = new System.Windows.Forms.Label();
            this.inSelectionLabel = new System.Windows.Forms.Label();
            this.numberOfSongsLabel = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.autosSaveTimer = new System.Windows.Forms.Timer(this.components);
            this.mediaPlayer = new AxWMPLib.AxWindowsMediaPlayer();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.DAWIcon = new System.Windows.Forms.DataGridViewImageColumn();
            this.RewireImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.RatingImage = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.musikantDataGridView)).BeginInit();
            this.gridContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.musikantDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editTempoUpDown)).BeginInit();
            this.menuStrip.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).BeginInit();
            this.SuspendLayout();
            // 
            // musikantDataGridView
            // 
            this.musikantDataGridView.AllowUserToAddRows = false;
            this.musikantDataGridView.AllowUserToDeleteRows = false;
            this.musikantDataGridView.AllowUserToResizeColumns = false;
            this.musikantDataGridView.AllowUserToResizeRows = false;
            this.musikantDataGridView.AutoGenerateColumns = false;
            this.musikantDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.musikantDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProjectID,
            this.NameText,
            this.Created,
            this.Modified,
            this.DAW,
            this.Rewire,
            this.Status,
            this.Potential,
            this.Ideas,
            this.Todos,
            this.Genre,
            this.Group,
            this.Tempo,
            this.DAWIcon,
            this.RewireImage,
            this.RatingImage});
            this.musikantDataGridView.ContextMenuStrip = this.gridContextMenuStrip;
            this.musikantDataGridView.DataSource = this.projectBindingSource;
            this.musikantDataGridView.Location = new System.Drawing.Point(0, 27);
            this.musikantDataGridView.Name = "musikantDataGridView";
            this.musikantDataGridView.ReadOnly = true;
            this.musikantDataGridView.RowHeadersVisible = false;
            this.musikantDataGridView.Size = new System.Drawing.Size(960, 558);
            this.musikantDataGridView.TabIndex = 1;
            this.musikantDataGridView.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.musikantDataGridView_CellDoubleClick);
            this.musikantDataGridView.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.musikantDataGridView_CellFormatting);
            this.musikantDataGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.musikantDataGridView_ColumnHeaderMouseClick);
            this.musikantDataGridView.SelectionChanged += new System.EventHandler(this.musikantDataGridView_SelectionChanged);
            // 
            // ProjectID
            // 
            this.ProjectID.DataPropertyName = "ProjectID";
            this.ProjectID.HeaderText = "ProjectID";
            this.ProjectID.Name = "ProjectID";
            this.ProjectID.ReadOnly = true;
            this.ProjectID.Visible = false;
            // 
            // NameText
            // 
            this.NameText.DataPropertyName = "Name";
            this.NameText.HeaderText = "Name";
            this.NameText.Name = "NameText";
            this.NameText.ReadOnly = true;
            this.NameText.Width = 240;
            // 
            // Created
            // 
            this.Created.DataPropertyName = "Created";
            this.Created.HeaderText = "Created";
            this.Created.Name = "Created";
            this.Created.ReadOnly = true;
            this.Created.Visible = false;
            // 
            // Modified
            // 
            this.Modified.DataPropertyName = "Modified";
            this.Modified.HeaderText = "Modified";
            this.Modified.Name = "Modified";
            this.Modified.ReadOnly = true;
            this.Modified.Width = 110;
            // 
            // DAW
            // 
            this.DAW.DataPropertyName = "DAW";
            this.DAW.HeaderText = "DAW";
            this.DAW.Name = "DAW";
            this.DAW.ReadOnly = true;
            this.DAW.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DAW.Visible = false;
            this.DAW.Width = 90;
            // 
            // Rewire
            // 
            this.Rewire.DataPropertyName = "Rewire";
            this.Rewire.HeaderText = "Rewire";
            this.Rewire.Name = "Rewire";
            this.Rewire.ReadOnly = true;
            this.Rewire.Visible = false;
            this.Rewire.Width = 80;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            this.Status.Visible = false;
            // 
            // Potential
            // 
            this.Potential.DataPropertyName = "Potential";
            this.Potential.HeaderText = "Potential";
            this.Potential.Name = "Potential";
            this.Potential.ReadOnly = true;
            this.Potential.Visible = false;
            // 
            // Ideas
            // 
            this.Ideas.DataPropertyName = "Ideas";
            this.Ideas.HeaderText = "Ideas";
            this.Ideas.Name = "Ideas";
            this.Ideas.ReadOnly = true;
            this.Ideas.Visible = false;
            // 
            // Todos
            // 
            this.Todos.DataPropertyName = "Todos";
            this.Todos.HeaderText = "Todos";
            this.Todos.Name = "Todos";
            this.Todos.ReadOnly = true;
            this.Todos.Visible = false;
            // 
            // Genre
            // 
            this.Genre.DataPropertyName = "Genre";
            this.Genre.HeaderText = "Genre";
            this.Genre.Name = "Genre";
            this.Genre.ReadOnly = true;
            // 
            // Group
            // 
            this.Group.DataPropertyName = "Group";
            this.Group.HeaderText = "Group";
            this.Group.Name = "Group";
            this.Group.ReadOnly = true;
            this.Group.Width = 160;
            // 
            // Tempo
            // 
            this.Tempo.DataPropertyName = "Tempo";
            this.Tempo.HeaderText = "Tempo";
            this.Tempo.Name = "Tempo";
            this.Tempo.ReadOnly = true;
            this.Tempo.Width = 50;
            // 
            // gridContextMenuStrip
            // 
            this.gridContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.masteringMenuItem,
            this.deleteProjectMenuItem,
            this.renameProjectMenuItem});
            this.gridContextMenuStrip.Name = "gridContextMenuStrip";
            this.gridContextMenuStrip.Size = new System.Drawing.Size(219, 70);
            // 
            // masteringMenuItem
            // 
            this.masteringMenuItem.Name = "masteringMenuItem";
            this.masteringMenuItem.Size = new System.Drawing.Size(218, 22);
            this.masteringMenuItem.Text = "Create Mastering Subfolder";
            this.masteringMenuItem.Click += new System.EventHandler(this.masteringMenuItem_Click);
            // 
            // deleteProjectMenuItem
            // 
            this.deleteProjectMenuItem.Name = "deleteProjectMenuItem";
            this.deleteProjectMenuItem.Size = new System.Drawing.Size(218, 22);
            this.deleteProjectMenuItem.Text = "Delete project";
            this.deleteProjectMenuItem.Visible = false;
            this.deleteProjectMenuItem.Click += new System.EventHandler(this.deleteProjectMenuItem_Click);
            // 
            // renameProjectMenuItem
            // 
            this.renameProjectMenuItem.Name = "renameProjectMenuItem";
            this.renameProjectMenuItem.Size = new System.Drawing.Size(218, 22);
            this.renameProjectMenuItem.Text = "Rename project";
            this.renameProjectMenuItem.Click += new System.EventHandler(this.renameProjectMenuItem_Click);
            // 
            // projectBindingSource
            // 
            this.projectBindingSource.DataMember = "Project";
            this.projectBindingSource.DataSource = this.musikantDataSet;
            // 
            // musikantDataSet
            // 
            this.musikantDataSet.DataSetName = "MusikantDataSet";
            this.musikantDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rewireCheckBox);
            this.groupBox1.Controls.Add(this.btnMemory2);
            this.groupBox1.Controls.Add(this.btnMemory4);
            this.groupBox1.Controls.Add(this.btnMemory3);
            this.groupBox1.Controls.Add(this.btnMemory1);
            this.groupBox1.Controls.Add(this.btnSet4);
            this.groupBox1.Controls.Add(this.btnSet3);
            this.groupBox1.Controls.Add(this.btnSet2);
            this.groupBox1.Controls.Add(this.btnSet1);
            this.groupBox1.Controls.Add(this.resetButton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.potentialComboBox);
            this.groupBox1.Controls.Add(this.tempoComboBox);
            this.groupBox1.Controls.Add(this.groupComboBox);
            this.groupBox1.Controls.Add(this.statusComboBox);
            this.groupBox1.Controls.Add(this.genreComboBox);
            this.groupBox1.Controls.Add(this.filterTextBox);
            this.groupBox1.Controls.Add(this.modifiedComboBox);
            this.groupBox1.Controls.Add(this.dawComboBox);
            this.groupBox1.Location = new System.Drawing.Point(969, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(230, 391);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filters";
            // 
            // rewireCheckBox
            // 
            this.rewireCheckBox.AutoSize = true;
            this.rewireCheckBox.Location = new System.Drawing.Point(62, 68);
            this.rewireCheckBox.Name = "rewireCheckBox";
            this.rewireCheckBox.Size = new System.Drawing.Size(15, 14);
            this.rewireCheckBox.TabIndex = 6;
            this.rewireCheckBox.UseVisualStyleBackColor = true;
            this.rewireCheckBox.CheckedChanged += new System.EventHandler(this.rewireCheckBox_CheckedChanged);
            // 
            // btnMemory2
            // 
            this.btnMemory2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMemory2.Location = new System.Drawing.Point(39, 311);
            this.btnMemory2.Name = "btnMemory2";
            this.btnMemory2.Size = new System.Drawing.Size(179, 23);
            this.btnMemory2.TabIndex = 5;
            this.btnMemory2.UseVisualStyleBackColor = true;
            this.btnMemory2.Click += new System.EventHandler(this.btnMemory2_Click);
            // 
            // btnMemory4
            // 
            this.btnMemory4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMemory4.Location = new System.Drawing.Point(39, 357);
            this.btnMemory4.Name = "btnMemory4";
            this.btnMemory4.Size = new System.Drawing.Size(179, 23);
            this.btnMemory4.TabIndex = 5;
            this.btnMemory4.UseVisualStyleBackColor = true;
            this.btnMemory4.Click += new System.EventHandler(this.btnMemory4_Click);
            // 
            // btnMemory3
            // 
            this.btnMemory3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMemory3.Location = new System.Drawing.Point(39, 334);
            this.btnMemory3.Name = "btnMemory3";
            this.btnMemory3.Size = new System.Drawing.Size(179, 23);
            this.btnMemory3.TabIndex = 5;
            this.btnMemory3.UseVisualStyleBackColor = true;
            this.btnMemory3.Click += new System.EventHandler(this.btnMemory3_Click);
            // 
            // btnMemory1
            // 
            this.btnMemory1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMemory1.Location = new System.Drawing.Point(39, 288);
            this.btnMemory1.Name = "btnMemory1";
            this.btnMemory1.Size = new System.Drawing.Size(179, 23);
            this.btnMemory1.TabIndex = 5;
            this.btnMemory1.UseVisualStyleBackColor = true;
            this.btnMemory1.Click += new System.EventHandler(this.btnMemory1_Click);
            // 
            // btnSet4
            // 
            this.btnSet4.Location = new System.Drawing.Point(9, 357);
            this.btnSet4.Name = "btnSet4";
            this.btnSet4.Size = new System.Drawing.Size(32, 23);
            this.btnSet4.TabIndex = 5;
            this.btnSet4.Text = "Set";
            this.btnSet4.UseVisualStyleBackColor = true;
            this.btnSet4.Click += new System.EventHandler(this.btnSet4_Click);
            // 
            // btnSet3
            // 
            this.btnSet3.Location = new System.Drawing.Point(9, 334);
            this.btnSet3.Name = "btnSet3";
            this.btnSet3.Size = new System.Drawing.Size(32, 23);
            this.btnSet3.TabIndex = 5;
            this.btnSet3.Text = "Set";
            this.btnSet3.UseVisualStyleBackColor = true;
            this.btnSet3.Click += new System.EventHandler(this.btnSet3_Click);
            // 
            // btnSet2
            // 
            this.btnSet2.Location = new System.Drawing.Point(9, 311);
            this.btnSet2.Name = "btnSet2";
            this.btnSet2.Size = new System.Drawing.Size(32, 23);
            this.btnSet2.TabIndex = 5;
            this.btnSet2.Text = "Set";
            this.btnSet2.UseVisualStyleBackColor = true;
            this.btnSet2.Click += new System.EventHandler(this.btnSet2_Click);
            // 
            // btnSet1
            // 
            this.btnSet1.Location = new System.Drawing.Point(9, 288);
            this.btnSet1.Name = "btnSet1";
            this.btnSet1.Size = new System.Drawing.Size(32, 23);
            this.btnSet1.TabIndex = 5;
            this.btnSet1.Text = "Set";
            this.btnSet1.UseVisualStyleBackColor = true;
            this.btnSet1.Click += new System.EventHandler(this.btnSet1_Click);
            // 
            // resetButton
            // 
            this.resetButton.Location = new System.Drawing.Point(61, 245);
            this.resetButton.Name = "resetButton";
            this.resetButton.Size = new System.Drawing.Size(158, 23);
            this.resetButton.TabIndex = 5;
            this.resetButton.Text = "Reset";
            this.resetButton.UseVisualStyleBackColor = true;
            this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Name:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 68);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 4;
            this.label16.Text = "Rewire:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Potential:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(11, 191);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(43, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "Tempo:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 215);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Group:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Progress:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Genre:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Modified:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "DAW:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // potentialComboBox
            // 
            this.potentialComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.potentialComboBox.FormattingEnabled = true;
            this.potentialComboBox.Items.AddRange(new object[] {
            global::Musikant.Properties.Resources.Reason,
            "Unknown",
            "Trash",
            "Boring",
            "Average",
            "Good",
            "Top"});
            this.potentialComboBox.Location = new System.Drawing.Point(60, 139);
            this.potentialComboBox.Name = "potentialComboBox";
            this.potentialComboBox.Size = new System.Drawing.Size(159, 21);
            this.potentialComboBox.TabIndex = 3;
            this.potentialComboBox.SelectedIndexChanged += new System.EventHandler(this.potentialComboBox_SelectedIndexChanged);
            // 
            // tempoComboBox
            // 
            this.tempoComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.tempoComboBox.FormattingEnabled = true;
            this.tempoComboBox.Items.AddRange(new object[] {
            global::Musikant.Properties.Resources.Reason,
            "31-80",
            "81-100",
            "101-120",
            "121-140",
            "141-250"});
            this.tempoComboBox.Location = new System.Drawing.Point(60, 188);
            this.tempoComboBox.Name = "tempoComboBox";
            this.tempoComboBox.Size = new System.Drawing.Size(159, 21);
            this.tempoComboBox.TabIndex = 3;
            this.tempoComboBox.SelectedIndexChanged += new System.EventHandler(this.tempoComboBox_SelectedIndexChanged);
            // 
            // groupComboBox
            // 
            this.groupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(60, 213);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(159, 21);
            this.groupComboBox.TabIndex = 3;
            this.groupComboBox.SelectedIndexChanged += new System.EventHandler(this.groupComboBox_SelectedIndexChanged);
            // 
            // statusComboBox
            // 
            this.statusComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.statusComboBox.FormattingEnabled = true;
            this.statusComboBox.Items.AddRange(new object[] {
            global::Musikant.Properties.Resources.Reason,
            "Unknown",
            "Idea",
            "Simple",
            "Advanced",
            "Finalize",
            "Done"});
            this.statusComboBox.Location = new System.Drawing.Point(60, 163);
            this.statusComboBox.Name = "statusComboBox";
            this.statusComboBox.Size = new System.Drawing.Size(159, 21);
            this.statusComboBox.TabIndex = 3;
            this.statusComboBox.SelectedIndexChanged += new System.EventHandler(this.statusComboBox_SelectedIndexChanged);
            // 
            // genreComboBox
            // 
            this.genreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.genreComboBox.FormattingEnabled = true;
            this.genreComboBox.Location = new System.Drawing.Point(61, 113);
            this.genreComboBox.Name = "genreComboBox";
            this.genreComboBox.Size = new System.Drawing.Size(159, 21);
            this.genreComboBox.TabIndex = 3;
            this.genreComboBox.SelectedIndexChanged += new System.EventHandler(this.genreComboBox_SelectedIndexChanged);
            // 
            // filterTextBox
            // 
            this.filterTextBox.Location = new System.Drawing.Point(61, 16);
            this.filterTextBox.Name = "filterTextBox";
            this.filterTextBox.Size = new System.Drawing.Size(160, 20);
            this.filterTextBox.TabIndex = 2;
            this.filterTextBox.TextChanged += new System.EventHandler(this.filterTextBox_TextChanged);
            // 
            // modifiedComboBox
            // 
            this.modifiedComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.modifiedComboBox.FormattingEnabled = true;
            this.modifiedComboBox.Items.AddRange(new object[] {
            global::Musikant.Properties.Resources.Reason,
            "yesterday",
            "last 3 days",
            "last week",
            "last 2 weeks",
            "last month",
            "last 3 months",
            "last 6 months",
            "last year"});
            this.modifiedComboBox.Location = new System.Drawing.Point(61, 87);
            this.modifiedComboBox.Name = "modifiedComboBox";
            this.modifiedComboBox.Size = new System.Drawing.Size(159, 21);
            this.modifiedComboBox.TabIndex = 1;
            this.modifiedComboBox.SelectedIndexChanged += new System.EventHandler(this.modifiedComboBox_SelectedIndexChanged);
            // 
            // dawComboBox
            // 
            this.dawComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dawComboBox.FormattingEnabled = true;
            this.dawComboBox.Items.AddRange(new object[] {
            global::Musikant.Properties.Resources.Reason,
            "Unknown",
            "Ableton Live",
            "Cubase",
            "FL Studio",
            "Reaper",
            "Reason",
            "Renoise",
            "Studio One"});
            this.dawComboBox.Location = new System.Drawing.Point(61, 42);
            this.dawComboBox.Name = "dawComboBox";
            this.dawComboBox.Size = new System.Drawing.Size(159, 21);
            this.dawComboBox.TabIndex = 0;
            this.dawComboBox.SelectedIndexChanged += new System.EventHandler(this.dawComboBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupsListBox);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.editTempoUpDown);
            this.groupBox2.Controls.Add(this.editRewireCheckBox);
            this.groupBox2.Controls.Add(this.todoTextBox);
            this.groupBox2.Controls.Add(this.infosTextBox);
            this.groupBox2.Controls.Add(this.ideasTextBox);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.filesTreeView);
            this.groupBox2.Controls.Add(this.fileInfoLabel);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.editDawComboBox);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.editGenreComboBox);
            this.groupBox2.Controls.Add(this.editStatusComboBox);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.editPotentialComboBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 596);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1187, 434);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Project Settings";
            // 
            // groupsListBox
            // 
            this.groupsListBox.CheckOnClick = true;
            this.groupsListBox.FormattingEnabled = true;
            this.groupsListBox.Location = new System.Drawing.Point(60, 153);
            this.groupsListBox.Name = "groupsListBox";
            this.groupsListBox.Size = new System.Drawing.Size(192, 94);
            this.groupsListBox.TabIndex = 13;
            this.groupsListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.groupsListBox_ItemCheck);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label28);
            this.groupBox4.Controls.Add(this.tempo4HzTextBox);
            this.groupBox4.Controls.Add(this.tempo16HzTextBox);
            this.groupBox4.Controls.Add(this.tempo4MsTextBox);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.tempo8MsTextBox);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.tempo1MsTextBox);
            this.groupBox4.Controls.Add(this.tempo4dMsTextBox);
            this.groupBox4.Controls.Add(this.tempo16MsTextBox);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Location = new System.Drawing.Point(60, 283);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(147, 134);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Timings";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(23, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(45, 13);
            this.label23.TabIndex = 11;
            this.label23.Text = "1 Cycle:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(18, 38);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(50, 13);
            this.label28.TabIndex = 11;
            this.label28.Text = "4 Cycles:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tempo4HzTextBox
            // 
            this.tempo4HzTextBox.AutoSize = true;
            this.tempo4HzTextBox.Location = new System.Drawing.Point(67, 23);
            this.tempo4HzTextBox.Name = "tempo4HzTextBox";
            this.tempo4HzTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo4HzTextBox.TabIndex = 11;
            this.tempo4HzTextBox.Text = "Hz";
            // 
            // tempo16HzTextBox
            // 
            this.tempo16HzTextBox.AutoSize = true;
            this.tempo16HzTextBox.Location = new System.Drawing.Point(67, 38);
            this.tempo16HzTextBox.Name = "tempo16HzTextBox";
            this.tempo16HzTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo16HzTextBox.TabIndex = 11;
            this.tempo16HzTextBox.Text = "Hz";
            // 
            // tempo4MsTextBox
            // 
            this.tempo4MsTextBox.AutoSize = true;
            this.tempo4MsTextBox.Location = new System.Drawing.Point(67, 87);
            this.tempo4MsTextBox.Name = "tempo4MsTextBox";
            this.tempo4MsTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo4MsTextBox.TabIndex = 11;
            this.tempo4MsTextBox.Text = "ms";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(41, 56);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(27, 13);
            this.label30.TabIndex = 11;
            this.label30.Text = "4/4:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(41, 72);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(27, 13);
            this.label27.TabIndex = 11;
            this.label27.Text = "3/8:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tempo8MsTextBox
            // 
            this.tempo8MsTextBox.AutoSize = true;
            this.tempo8MsTextBox.Location = new System.Drawing.Point(67, 102);
            this.tempo8MsTextBox.Name = "tempo8MsTextBox";
            this.tempo8MsTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo8MsTextBox.TabIndex = 11;
            this.tempo8MsTextBox.Text = "ms";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(35, 117);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(33, 13);
            this.label26.TabIndex = 11;
            this.label26.Text = "1/16:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(41, 87);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(27, 13);
            this.label24.TabIndex = 11;
            this.label24.Text = "1/4:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tempo1MsTextBox
            // 
            this.tempo1MsTextBox.AutoSize = true;
            this.tempo1MsTextBox.Location = new System.Drawing.Point(67, 56);
            this.tempo1MsTextBox.Name = "tempo1MsTextBox";
            this.tempo1MsTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo1MsTextBox.TabIndex = 11;
            this.tempo1MsTextBox.Text = "ms";
            // 
            // tempo4dMsTextBox
            // 
            this.tempo4dMsTextBox.AutoSize = true;
            this.tempo4dMsTextBox.Location = new System.Drawing.Point(67, 72);
            this.tempo4dMsTextBox.Name = "tempo4dMsTextBox";
            this.tempo4dMsTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo4dMsTextBox.TabIndex = 11;
            this.tempo4dMsTextBox.Text = "ms";
            // 
            // tempo16MsTextBox
            // 
            this.tempo16MsTextBox.AutoSize = true;
            this.tempo16MsTextBox.Location = new System.Drawing.Point(67, 117);
            this.tempo16MsTextBox.Name = "tempo16MsTextBox";
            this.tempo16MsTextBox.Size = new System.Drawing.Size(20, 13);
            this.tempo16MsTextBox.TabIndex = 11;
            this.tempo16MsTextBox.Text = "ms";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(41, 102);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(27, 13);
            this.label25.TabIndex = 11;
            this.label25.Text = "1/8:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // editTempoUpDown
            // 
            this.editTempoUpDown.Location = new System.Drawing.Point(60, 257);
            this.editTempoUpDown.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.editTempoUpDown.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.editTempoUpDown.Name = "editTempoUpDown";
            this.editTempoUpDown.Size = new System.Drawing.Size(50, 20);
            this.editTempoUpDown.TabIndex = 10;
            this.editTempoUpDown.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.editTempoUpDown.ValueChanged += new System.EventHandler(this.editTempoUpDown_ValueChanged);
            // 
            // editRewireCheckBox
            // 
            this.editRewireCheckBox.AutoSize = true;
            this.editRewireCheckBox.Location = new System.Drawing.Point(60, 57);
            this.editRewireCheckBox.Name = "editRewireCheckBox";
            this.editRewireCheckBox.Size = new System.Drawing.Size(15, 14);
            this.editRewireCheckBox.TabIndex = 6;
            this.editRewireCheckBox.UseVisualStyleBackColor = true;
            this.editRewireCheckBox.CheckedChanged += new System.EventHandler(this.editRewireCheckBox_CheckedChanged);
            // 
            // todoTextBox
            // 
            this.todoTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.todoTextBox.Location = new System.Drawing.Point(261, 304);
            this.todoTextBox.Multiline = true;
            this.todoTextBox.Name = "todoTextBox";
            this.todoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.todoTextBox.Size = new System.Drawing.Size(477, 124);
            this.todoTextBox.TabIndex = 1;
            this.todoTextBox.TextChanged += new System.EventHandler(this.todoTextBox_TextChanged);
            // 
            // infosTextBox
            // 
            this.infosTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infosTextBox.Location = new System.Drawing.Point(261, 35);
            this.infosTextBox.Multiline = true;
            this.infosTextBox.Name = "infosTextBox";
            this.infosTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.infosTextBox.Size = new System.Drawing.Size(477, 115);
            this.infosTextBox.TabIndex = 1;
            this.infosTextBox.TextChanged += new System.EventHandler(this.infosTextBox_TextChanged);
            // 
            // ideasTextBox
            // 
            this.ideasTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ideasTextBox.Location = new System.Drawing.Point(261, 169);
            this.ideasTextBox.Multiline = true;
            this.ideasTextBox.Name = "ideasTextBox";
            this.ideasTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ideasTextBox.Size = new System.Drawing.Size(477, 116);
            this.ideasTextBox.TabIndex = 1;
            this.ideasTextBox.TextChanged += new System.EventHandler(this.ideasTextBox_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 57);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(43, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Rewire:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // filesTreeView
            // 
            this.filesTreeView.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.filesTreeView.ImageIndex = 0;
            this.filesTreeView.ImageList = this.musikantImageList;
            this.filesTreeView.LabelEdit = true;
            this.filesTreeView.Location = new System.Drawing.Point(744, 35);
            this.filesTreeView.Name = "filesTreeView";
            this.filesTreeView.SelectedImageIndex = 0;
            this.filesTreeView.Size = new System.Drawing.Size(435, 360);
            this.filesTreeView.TabIndex = 5;
            this.filesTreeView.AfterLabelEdit += new System.Windows.Forms.NodeLabelEditEventHandler(this.filesTreeView_AfterLabelEdit);
            this.filesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.filesTreeView_AfterSelect);
            this.filesTreeView.DoubleClick += new System.EventHandler(this.filesTreeView_DoubleClick);
            this.filesTreeView.KeyDown += new System.Windows.Forms.KeyEventHandler(this.filesTreeView_KeyDown);
            // 
            // musikantImageList
            // 
            this.musikantImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("musikantImageList.ImageStream")));
            this.musikantImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.musikantImageList.Images.SetKeyName(0, "Ableton Live");
            this.musikantImageList.Images.SetKeyName(1, "Cubase");
            this.musikantImageList.Images.SetKeyName(2, "FL Studio");
            this.musikantImageList.Images.SetKeyName(3, "Reaper");
            this.musikantImageList.Images.SetKeyName(4, "Reason");
            this.musikantImageList.Images.SetKeyName(5, "Sound");
            this.musikantImageList.Images.SetKeyName(6, "Studio One");
            this.musikantImageList.Images.SetKeyName(7, "Renoise");
            this.musikantImageList.Images.SetKeyName(8, "File");
            this.musikantImageList.Images.SetKeyName(9, "Folder");
            // 
            // fileInfoLabel
            // 
            this.fileInfoLabel.AutoSize = true;
            this.fileInfoLabel.Location = new System.Drawing.Point(741, 400);
            this.fileInfoLabel.Name = "fileInfoLabel";
            this.fileInfoLabel.Size = new System.Drawing.Size(44, 13);
            this.fileInfoLabel.TabIndex = 4;
            this.fileInfoLabel.Text = "File Info";
            this.fileInfoLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "Potential:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(8, 259);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(43, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Tempo:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Group:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 128);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 4;
            this.label9.Text = "Progress:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // editDawComboBox
            // 
            this.editDawComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.editDawComboBox.FormattingEnabled = true;
            this.editDawComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Ableton Live",
            "Cubase",
            "FL Studio",
            "Reaper",
            "Reason",
            "Renoise",
            "Studio One"});
            this.editDawComboBox.Location = new System.Drawing.Point(60, 30);
            this.editDawComboBox.Name = "editDawComboBox";
            this.editDawComboBox.Size = new System.Drawing.Size(192, 21);
            this.editDawComboBox.TabIndex = 0;
            this.editDawComboBox.SelectedIndexChanged += new System.EventHandler(this.editDawComboBox_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "DAW:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Genre:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // editGenreComboBox
            // 
            this.editGenreComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.editGenreComboBox.FormattingEnabled = true;
            this.editGenreComboBox.Location = new System.Drawing.Point(60, 76);
            this.editGenreComboBox.Name = "editGenreComboBox";
            this.editGenreComboBox.Size = new System.Drawing.Size(192, 21);
            this.editGenreComboBox.TabIndex = 3;
            this.editGenreComboBox.SelectedIndexChanged += new System.EventHandler(this.editGenreComboBox_SelectedIndexChanged);
            // 
            // editStatusComboBox
            // 
            this.editStatusComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.editStatusComboBox.FormattingEnabled = true;
            this.editStatusComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Idea",
            "Simple",
            "Advanced",
            "Finalize",
            "Done"});
            this.editStatusComboBox.Location = new System.Drawing.Point(60, 125);
            this.editStatusComboBox.Name = "editStatusComboBox";
            this.editStatusComboBox.Size = new System.Drawing.Size(192, 21);
            this.editStatusComboBox.TabIndex = 3;
            this.editStatusComboBox.SelectedIndexChanged += new System.EventHandler(this.editStatusComboBox_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(258, 288);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(42, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "ToDos:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(741, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "Files:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(258, 19);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Infos:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(258, 153);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Ideas:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // editPotentialComboBox
            // 
            this.editPotentialComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.editPotentialComboBox.FormattingEnabled = true;
            this.editPotentialComboBox.Items.AddRange(new object[] {
            "Unknown",
            "Trash",
            "Boring",
            "Average",
            "Good",
            "Top"});
            this.editPotentialComboBox.Location = new System.Drawing.Point(60, 100);
            this.editPotentialComboBox.Name = "editPotentialComboBox";
            this.editPotentialComboBox.Size = new System.Drawing.Size(192, 21);
            this.editPotentialComboBox.TabIndex = 3;
            this.editPotentialComboBox.SelectedIndexChanged += new System.EventHandler(this.editPotentialComboBox_SelectedIndexChanged);
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1199, 24);
            this.menuStrip.TabIndex = 4;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveMenuItem,
            this.rescanMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.Size = new System.Drawing.Size(201, 22);
            this.saveMenuItem.Text = "Save data";
            this.saveMenuItem.Click += new System.EventHandler(this.saveMenuItem_Click);
            // 
            // rescanMenuItem
            // 
            this.rescanMenuItem.Name = "rescanMenuItem";
            this.rescanMenuItem.Size = new System.Drawing.Size(201, 22);
            this.rescanMenuItem.Text = "Rescan project directory";
            this.rescanMenuItem.Click += new System.EventHandler(this.rescanMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.newProjNumLabel);
            this.groupBox3.Controls.Add(this.inSelectionLabel);
            this.groupBox3.Controls.Add(this.numberOfSongsLabel);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Location = new System.Drawing.Point(969, 424);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(230, 88);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Infos";
            // 
            // newProjNumLabel
            // 
            this.newProjNumLabel.AutoSize = true;
            this.newProjNumLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newProjNumLabel.Location = new System.Drawing.Point(152, 65);
            this.newProjNumLabel.Name = "newProjNumLabel";
            this.newProjNumLabel.Size = new System.Drawing.Size(14, 13);
            this.newProjNumLabel.TabIndex = 1;
            this.newProjNumLabel.Text = "0";
            // 
            // inSelectionLabel
            // 
            this.inSelectionLabel.AutoSize = true;
            this.inSelectionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inSelectionLabel.Location = new System.Drawing.Point(152, 43);
            this.inSelectionLabel.Name = "inSelectionLabel";
            this.inSelectionLabel.Size = new System.Drawing.Size(14, 13);
            this.inSelectionLabel.TabIndex = 1;
            this.inSelectionLabel.Text = "0";
            // 
            // numberOfSongsLabel
            // 
            this.numberOfSongsLabel.AutoSize = true;
            this.numberOfSongsLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numberOfSongsLabel.Location = new System.Drawing.Point(152, 20);
            this.numberOfSongsLabel.Name = "numberOfSongsLabel";
            this.numberOfSongsLabel.Size = new System.Drawing.Size(14, 13);
            this.numberOfSongsLabel.TabIndex = 1;
            this.numberOfSongsLabel.Text = "0";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(11, 65);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(105, 13);
            this.label31.TabIndex = 0;
            this.label31.Text = "New project number:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 43);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(134, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Filtered number of projects:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(124, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Total number of projects:";
            // 
            // autosSaveTimer
            // 
            this.autosSaveTimer.Enabled = true;
            this.autosSaveTimer.Interval = 600000;
            this.autosSaveTimer.Tick += new System.EventHandler(this.autosSaveTimer_Tick);
            // 
            // mediaPlayer
            // 
            this.mediaPlayer.Enabled = true;
            this.mediaPlayer.Location = new System.Drawing.Point(969, 519);
            this.mediaPlayer.Name = "mediaPlayer";
            this.mediaPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mediaPlayer.OcxState")));
            this.mediaPlayer.Size = new System.Drawing.Size(230, 66);
            this.mediaPlayer.TabIndex = 6;
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "Rewire";
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 50;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.HeaderText = "Rating";
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.ReadOnly = true;
            this.dataGridViewImageColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn2.Width = 50;
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.HeaderText = "Potential";
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.ReadOnly = true;
            // 
            // DAWIcon
            // 
            this.DAWIcon.HeaderText = "DAW";
            this.DAWIcon.Name = "DAWIcon";
            this.DAWIcon.ReadOnly = true;
            this.DAWIcon.Width = 40;
            // 
            // RewireImage
            // 
            this.RewireImage.HeaderText = "Rewire";
            this.RewireImage.Name = "RewireImage";
            this.RewireImage.ReadOnly = true;
            this.RewireImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.RewireImage.Width = 50;
            // 
            // RatingImage
            // 
            this.RatingImage.HeaderText = "Potential";
            this.RatingImage.Name = "RatingImage";
            this.RatingImage.ReadOnly = true;
            // 
            // MusikantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1199, 1030);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.mediaPlayer);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.musikantDataGridView);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.groupBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MusikantForm";
            this.Text = "Musikant";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MusikantForm_FormClosing);
            this.Load += new System.EventHandler(this.MusikantForm_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MusikantForm_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.musikantDataGridView)).EndInit();
            this.gridContextMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.projectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.musikantDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editTempoUpDown)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mediaPlayer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView musikantDataGridView;
        private System.Windows.Forms.BindingSource projectBindingSource;
        private Data.MusikantDataSet musikantDataSet;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox dawComboBox;
        private System.Windows.Forms.ComboBox modifiedComboBox;
        private System.Windows.Forms.TextBox filterTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox genreComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox potentialComboBox;
        private System.Windows.Forms.ComboBox statusComboBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox editDawComboBox;
        private System.Windows.Forms.TextBox ideasTextBox;
        private System.Windows.Forms.ComboBox editGenreComboBox;
        private System.Windows.Forms.ComboBox editStatusComboBox;
        private System.Windows.Forms.ComboBox editPotentialComboBox;
        private System.Windows.Forms.TextBox todoTextBox;
        private System.Windows.Forms.Button resetButton;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.TreeView filesTreeView;
        private AxWMPLib.AxWindowsMediaPlayer mediaPlayer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.Label fileInfoLabel;
        private System.Windows.Forms.CheckBox rewireCheckBox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox editRewireCheckBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.ImageList musikantImageList;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label numberOfSongsLabel;
        private System.Windows.Forms.TextBox infosTextBox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ContextMenuStrip gridContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem masteringMenuItem;
        private System.Windows.Forms.Label inSelectionLabel;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ToolStripMenuItem deleteProjectMenuItem;
        private System.Windows.Forms.Timer autosSaveTimer;
        private System.Windows.Forms.NumericUpDown editTempoUpDown;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label tempo16HzTextBox;
        private System.Windows.Forms.Label tempo4dMsTextBox;
        private System.Windows.Forms.Label tempo16MsTextBox;
        private System.Windows.Forms.Label tempo8MsTextBox;
        private System.Windows.Forms.Label tempo4MsTextBox;
        private System.Windows.Forms.Label tempo4HzTextBox;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox tempoComboBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label tempo1MsTextBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label newProjNumLabel;
        private System.Windows.Forms.Button btnMemory2;
        private System.Windows.Forms.Button btnMemory3;
        private System.Windows.Forms.Button btnMemory1;
        private System.Windows.Forms.Button btnSet3;
        private System.Windows.Forms.Button btnSet2;
        private System.Windows.Forms.Button btnSet1;
        private System.Windows.Forms.ToolStripMenuItem rescanMenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameProjectMenuItem;
        private System.Windows.Forms.CheckedListBox groupsListBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameText;
        private System.Windows.Forms.DataGridViewTextBoxColumn Created;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modified;
        private System.Windows.Forms.DataGridViewTextBoxColumn DAW;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rewire;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Potential;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ideas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Todos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Genre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tempo;
        private System.Windows.Forms.DataGridViewImageColumn DAWIcon;
        private System.Windows.Forms.DataGridViewImageColumn RewireImage;
        private System.Windows.Forms.DataGridViewImageColumn RatingImage;
        private System.Windows.Forms.Button btnMemory4;
        private System.Windows.Forms.Button btnSet4;
    }
}

