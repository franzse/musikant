﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Musikant.Data;
using Musikant.Utils;
using System.IO;
using Musikant.Facade;
using System.Diagnostics;
using Musikant.Forms;
using Musikant.Properties;

namespace Musikant
{
    public partial class MusikantForm : Form
    {

        MusikantDataSet dataSet;
        MusikantFacade facade;
        TypeAssistant assistant;
        MusikantDataSet.ProjectRow curProject;
        string xmlFile = string.Format(@"{0}\Musikant.xml", System.IO.Directory.GetCurrentDirectory());
        string filterDesc;
        string filter;
        Boolean starting;

        public MusikantForm()
        {
            InitializeComponent();
            assistant = new TypeAssistant();
            assistant.Idled += assistant_Idled;
            starting = true;
        }
        
        private void musikantDataGridView_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (musikantDataGridView.Columns[e.ColumnIndex].Name.Equals("NameText"))
            {
                DataGridViewRow curViewRow = musikantDataGridView.Rows[e.RowIndex];
                long curId = (long)curViewRow.Cells["ProjectID"].Value;

                if (facade.IsNewProject(curId))
                {
                    curViewRow.Cells["NameText"].Style.BackColor = Color.LightGreen; // Color.Salmon LightGreen;
                }
                else
                {
                    curViewRow.Cells["NameText"].Style.BackColor = Color.Empty;
                }

                if (Constants.UNKNOWN == curViewRow.Cells["DAW"].Value.ToString())
                {
                    curViewRow.Cells["DAW"].Style.BackColor = Color.Linen; 
                }
                else
                {
                    curViewRow.Cells["DAW"].Style.BackColor = Color.Empty;
                    Image dawIcon = Musikant.Properties.Resources.File;
                    switch (curViewRow.Cells["DAW"].Value.ToString())
                    {
                        case "Ableton Live":
                            dawIcon = Musikant.Properties.Resources.Ableton_Live;
                            break;
                        case "Cubase":
                            dawIcon = Musikant.Properties.Resources.Cubase;
                            break;
                        case "Reason":
                            dawIcon = Musikant.Properties.Resources.Reason1;
                            break;
                        case "Reaper":
                            dawIcon = Musikant.Properties.Resources.Reaper;
                            break;
                        case "Renoise":
                            dawIcon = Musikant.Properties.Resources.Renoise;
                            break;
                        case "FL Studio":
                            dawIcon = Musikant.Properties.Resources.FL_Studio;
                            break;
                        case "Studio One":
                            dawIcon = Musikant.Properties.Resources.Studio_One;
                            break;
                    }
                    curViewRow.Cells["DAWIcon"].Value = dawIcon;
                }

                if (Constants.UNKNOWN == curViewRow.Cells["Genre"].Value.ToString())
                {
                    curViewRow.Cells["Genre"].Style.BackColor = Color.Linen; 
                }
                else
                {
                    curViewRow.Cells["Genre"].Style.BackColor = Color.Empty;
                }

                int curStatus = (int)curViewRow.Cells["Status"].Value;
                int progress = 0;
                switch (curStatus)
                {
                    case 2:
                        progress = 20;
                        break;
                    case 3:
                        progress = 40;
                        break;
                    case 4:
                        progress = 60;
                        break;
                    case 5:
                        progress = 80;
                        break;
                    case 6:
                        progress = 100;
                        break;
                }
                curViewRow.Cells["Progress"].Value = progress;
                    
                int curPotential = (int)curViewRow.Cells["Potential"].Value;
                Image ratingImage = Musikant.Properties.Resources._0star;
                switch (curPotential)
                {
                    case 2:
                        ratingImage = Musikant.Properties.Resources._1star;
                        break;
                    case 3:
                        ratingImage = Musikant.Properties.Resources._2star;
                        break;
                    case 4:
                        ratingImage = Musikant.Properties.Resources._3star;
                        break;
                    case 5:
                        ratingImage = Musikant.Properties.Resources._4star;
                        break;
                    case 6:
                        ratingImage = Musikant.Properties.Resources._5star;
                        break;
                }
                curViewRow.Cells["RatingImage"].Value = ratingImage;

                if (Constants.TRUE == curViewRow.Cells["Rewire"].Value.ToString())
                {
                    curViewRow.Cells["RewireImage"].Value = Musikant.Properties.Resources.checkmark_16;
                }
                else
                {
                    curViewRow.Cells["RewireImage"].Value = Musikant.Properties.Resources.unchecked_16;
                }

                if ("30" == curViewRow.Cells["Tempo"].Value.ToString())
                {
                    curViewRow.Cells["Tempo"].Style.BackColor = Color.Linen;
                }
                else
                {
                    curViewRow.Cells["Tempo"].Style.BackColor = Color.Empty;
                }
            }
        }

        private void MusikantForm_Load(object sender, EventArgs e)
        {
            if ( ! new FileInfo(xmlFile).Exists)
            {
                MessageBoxEx.Show(this, "Configuration file 'Musikant.xml' does not exist.", "Musikant", MessageBoxButtons.OK);
                Environment.Exit(0);
            }
            dataSet = new MusikantDataSet();
            dataSet.CaseSensitive = false;
            try
            {
                dataSet.ReadXml(xmlFile, XmlReadMode.InferSchema);
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(this, "Error reading configuration file 'Musikant.xml': " + ex.Message, "Musikant", MessageBoxButtons.OK);
                Environment.Exit(0);
            }
            DirectoryInfo projectDir = new DirectoryInfo(dataSet.GetConfigValue(Constants.CONFIG_PROJECT_DIR));
            if ( ! projectDir.Exists)
            {
                MessageBoxEx.Show(this, "Project directory does not exist. Please edit configuration file.", "Musikant", MessageBoxButtons.OK);
                Environment.Exit(0);
            }

            // Groups auslesen
            BindingSource bsGroups = new BindingSource();
            bsGroups.DataSource = dataSet.GetConfigList(Constants.CONFIG_GROUPS, false, false);
            groupsListBox.DataSource = bsGroups;

            BindingSource bs2 = new BindingSource();
            bs2.DataSource = dataSet.GetConfigList(Constants.CONFIG_GROUPS, true, false);
            groupComboBox.DataSource = bs2;

            BindingSource genreBindingSource = new BindingSource();
            genreBindingSource.DataSource = dataSet.GetConfigList(Constants.CONFIG_GENRES, false, true);
            editGenreComboBox.DataSource = genreBindingSource;
            BindingSource filterGenreBindingSource = new BindingSource();
            filterGenreBindingSource.DataSource = dataSet.GetConfigList(Constants.CONFIG_GENRES, true, true);
            genreComboBox.DataSource = filterGenreBindingSource;

            facade = new MusikantFacade(dataSet);

            // Custom ProgressColumn
            DataGridViewProgressColumn progressColumn = new DataGridViewProgressColumn();
            progressColumn.Name = "Progress";
            progressColumn.Width = 90; // Standard 100
            progressColumn.HeaderText = "Progress";
            progressColumn.ProgressBarColor = Color.LightGreen;
            
            musikantDataGridView.Columns.Insert(14, progressColumn);
            musikantDataGridView.DataSource = dataSet.Project;
            
            fileInfoLabel.Text = "";
            numberOfSongsLabel.Text = dataSet.Project.Count.ToString();
            inSelectionLabel.Text = musikantDataGridView.RowCount.ToString();
            newProjNumLabel.Text = (dataSet.Project.Max(p => p.ProjectID) + 1).ToString(); ;

            // Set window location
            if (Settings.Default.WindowLocation != null)
            {
                this.Location = Settings.Default.WindowLocation;
            }

            if (dataSet.GetConfigValue("Filter1") != null)
            {
                btnMemory1.Text = shortenFilterDesc(dataSet.GetConfigValue("FilterDesc1"));
            }
            if (dataSet.GetConfigValue("Filter2") != null)
            {
                btnMemory2.Text = shortenFilterDesc(dataSet.GetConfigValue("FilterDesc2"));
            }
            if (dataSet.GetConfigValue("Filter3") != null)
            {
                btnMemory3.Text = shortenFilterDesc(dataSet.GetConfigValue("FilterDesc3"));
            }
            starting = false;
            filterDataGridView();
        }

        private void dawComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        void assistant_Idled(object sender, EventArgs e)
        {
            this.Invoke(
            new MethodInvoker(() =>
            {
                filterDataGridView();
            }));
        }

        private void filterTextBox_TextChanged(object sender, EventArgs e)
        {
            assistant.TextChanged();
        }

        private void filterDataGridView(object[] filterParams)
        {
            if (starting) return;

            List<string> filters = new List<string>();
            List<string> memory = new List<string>();
            string search = (string)filterParams[0];
            int daw = (int)filterParams[1];
            int genre = (int)filterParams[2];
            int modified = (int)filterParams[3];
            int status = (int)filterParams[4];
            int potential = (int)filterParams[5];
            int group = (int)filterParams[6];
            int tempo = (int)filterParams[7];
            bool rewire = (bool)filterParams[8];

            if (daw >= 0)
            {
                filters.Add(string.Format("DAW = '{0}'", dawComboBox.Items[daw].ToString()));
                memory.Add(dawComboBox.Items[daw].ToString());
            }
            if (genre > 0)
            {
                filters.Add(string.Format("Genre = '{0}'", genreComboBox.Items[genre].ToString()));
                memory.Add(genreComboBox.Items[genre].ToString());
            }
            if (modified >= 0)
            {
                DateTime filterDate = DateTime.Now;
                switch (modifiedComboBox.Items[modified].ToString())
                {
                    case "yesterday":
                        filterDate = DateTime.Now.AddDays(-1);
                        break;
                    case "last 3 days":
                        filterDate = DateTime.Now.AddDays(-3);
                        break;
                    case "last week":
                        filterDate = DateTime.Now.AddDays(-7);
                        break;
                    case "last 2 weeks":
                        filterDate = DateTime.Now.AddDays(-14);
                        break;
                    case "last month":
                        filterDate = DateTime.Now.AddMonths(-1);
                        break;
                    case "last 3 months":
                        filterDate = DateTime.Now.AddMonths(-3);
                        break;
                    case "last 6 months":
                        filterDate = DateTime.Now.AddMonths(-6);
                        break;
                    case "last year":
                        filterDate = DateTime.Now.AddYears(-1);
                        break;
                }
                filters.Add(string.Format("Modified > #{0}#", filterDate.ToString("yyyy-MM-dd")));
                memory.Add(modifiedComboBox.Items[modified].ToString());
            }
            if (status >= 0)
            {
                filters.Add(string.Format("Status = {0}", status));
                memory.Add(statusComboBox.Items[status].ToString());
            }
            if (potential >= 0)
            {
                filters.Add(string.Format("Potential >= {0}", potential));
                memory.Add(potentialComboBox.Items[potential].ToString());
            }
            if (search != "")
            {
                filters.Add(string.Format("(Name LIKE '%{0}%' OR Infos LIKE '%{0}%' OR Ideas LIKE '%{0}%' OR Todos LIKE '%{0}%')", search));
                memory.Add(search);
            }
            if (rewire)
            {
                filters.Add("Rewire = 'True'");
                memory.Add("Rewire");
            }
            if (group > 0)
            {
                filters.Add(string.Format("Group LIKE '%{0}%'", groupComboBox.Items[group].ToString()));
                memory.Add(groupComboBox.Items[group].ToString());
            }
            if (tempo >= 0)
            {
                decimal tempoStart = 31;
                decimal tempoEnd = 80;
                switch (tempoComboBox.Items[tempo].ToString())
                {
                    case "81-100":
                        tempoStart = 81;
                        tempoEnd = 100;
                        break;
                    case "101-120":
                        tempoStart = 101;
                        tempoEnd = 120;
                        break;
                    case "121-140":
                        tempoStart = 121;
                        tempoEnd = 140;
                        break;
                    case "141-250":
                        tempoStart = 141;
                        tempoEnd = 250;
                        break;
                }
                filters.Add(string.Format("(Tempo >= {0} AND Tempo <= {1})", tempoStart, tempoEnd));
                memory.Add(tempoComboBox.Items[tempo].ToString());
            }

            if (filters.Count > 0)
            {
                string filterStr = "";
                string memoryStr = "";
                for (int i = 0; i < filters.Count; i++)
                {
                    filterStr = filterStr + filters[i];
                    if (i < filters.Count - 1)
                    {
                        filterStr = filterStr + " AND ";
                    }
                    if (i > 0)
                    {
                        memoryStr = memoryStr + ",";
                    }
                    memoryStr = memoryStr + memory[i];
                }
                System.Console.WriteLine(memoryStr);
                dataSet.Project.DefaultView.RowFilter = filterStr;
                filterDesc = memoryStr;
                filter = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                    filterParams[0], filterParams[1], filterParams[2], filterParams[3],
                    filterParams[4], filterParams[5], filterParams[6], filterParams[7],
                    filterParams[8]);

            } else {
                dataSet.Project.DefaultView.RowFilter = string.Empty;
            }
            inSelectionLabel.Text = musikantDataGridView.RowCount.ToString();
        }

        private void filterDataGridView()
        {
            if (starting) return;

            object[] filterParams = {"", -1, -1, -1, -1, -1, -1, -1, false};
            filterParams[0] = filterTextBox.Text.Trim();
            filterParams[1] = dawComboBox.SelectedIndex;
            filterParams[2] = genreComboBox.SelectedIndex;
            filterParams[3] = modifiedComboBox.SelectedIndex;
            filterParams[4] = statusComboBox.SelectedIndex;
            filterParams[5] = potentialComboBox.SelectedIndex;
            filterParams[6] = groupComboBox.SelectedIndex;
            filterParams[7] = tempoComboBox.SelectedIndex;
            filterParams[8] = rewireCheckBox.Checked;

            filterDataGridView(filterParams);

            /*
            List<string> filters = new List<string>();
            List<string> memory = new List<string>();
            if (dawComboBox.SelectedItem != null && dawComboBox.SelectedItem.ToString() != "")
            {
                
                filters.Add(string.Format("DAW = '{0}'", dawComboBox.SelectedItem.ToString()));
                memory.Add(dawComboBox.SelectedItem.ToString());
            }
            if (genreComboBox.SelectedItem != null && genreComboBox.SelectedItem.ToString() != "")
            {
                filters.Add(string.Format("Genre = '{0}'", genreComboBox.SelectedItem.ToString()));
                memory.Add(genreComboBox.SelectedItem.ToString());
            }
            if (modifiedComboBox.SelectedItem != null && modifiedComboBox.SelectedItem.ToString() != "")
            {
                DateTime filterDate = DateTime.Now;
                switch (modifiedComboBox.SelectedItem.ToString()) {
                    case "yesterday":
                        filterDate = DateTime.Now.AddDays(-1);
                        break;
                    case "last 3 days":
                        filterDate = DateTime.Now.AddDays(-3);
                        break;
                    case "last week":
                        filterDate = DateTime.Now.AddDays(-7);
                        break;
                    case "last 2 weeks":
                        filterDate = DateTime.Now.AddDays(-14);
                        break;
                    case "last month":
                        filterDate = DateTime.Now.AddMonths(-1);
                        break;
                    case "last 3 months":
                        filterDate = DateTime.Now.AddMonths(-3);
                        break;
                    case "last 6 months":
                        filterDate = DateTime.Now.AddMonths(-6);
                        break;
                    case "last year":
                        filterDate = DateTime.Now.AddYears(-1);
                        break;
                }
                filters.Add(string.Format("Modified > #{0}#", filterDate.ToString("yyyy-MM-dd")));
                memory.Add(modifiedComboBox.SelectedItem.ToString());
            }
            if (statusComboBox.SelectedItem != null && statusComboBox.SelectedItem.ToString() != "")
            {
                filters.Add(string.Format("Status = {0}", statusComboBox.SelectedIndex));
                memory.Add(statusComboBox.SelectedItem.ToString());
            }
            if (potentialComboBox.SelectedItem != null && potentialComboBox.SelectedItem.ToString() != "")
            {
                filters.Add(string.Format("Potential >= {0}", potentialComboBox.SelectedIndex));
                memory.Add(potentialComboBox.SelectedItem.ToString());
            }
            if (filterTextBox.Text.Trim() != "")
            {
                filters.Add(string.Format("(Name LIKE '%{0}%' OR Infos LIKE '%{0}%' OR Ideas LIKE '%{0}%' OR Todos LIKE '%{0}%')", filterTextBox.Text.Trim()));
                memory.Add(filterTextBox.Text.Trim());
            }
            if (rewireCheckBox.Checked)
            {
                filters.Add("Rewire = 'True'");
                memory.Add("Rewire");
            }
            if (groupComboBox.SelectedItem != null && groupComboBox.SelectedItem.ToString() != "")
            {
                filters.Add(string.Format("Group LIKE '%{0}%'", groupComboBox.SelectedItem.ToString()));
                memory.Add(groupComboBox.SelectedItem.ToString());
            }
            if (tempoComboBox.SelectedItem != null && tempoComboBox.SelectedItem.ToString() != "")
            {
                decimal tempoStart = 31;
                decimal tempoEnd = 80;
                switch (tempoComboBox.SelectedItem.ToString())
                {
                    case "81-100":
                        tempoStart = 81;
                        tempoEnd = 100;
                        break;
                    case "101-120":
                        tempoStart = 101;
                        tempoEnd = 120;
                        break;
                    case "121-140":
                        tempoStart = 121;
                        tempoEnd = 140;
                        break;
                    case "141-250":
                        tempoStart = 141;
                        tempoEnd = 250;
                        break;
                }
                filters.Add(string.Format("(Tempo >= {0} AND Tempo <= {1})", tempoStart, tempoEnd));
                memory.Add(tempoComboBox.SelectedItem.ToString());
            }

            if (filters.Count > 0)
            {
                string filterStr = "";
                string memoryStr = "";
                for (int i = 0; i < filters.Count; i++)
                {
                   filterStr = filterStr + filters[i];
                   if (i < filters.Count - 1) {
                       filterStr = filterStr + " AND ";
                   }
                   if (i > 0)
                   {
                       memoryStr = memoryStr + ",";
                   }
                   memoryStr = memoryStr + memory[i];
                }
                System.Console.WriteLine(memoryStr);
                dataSet.Project.DefaultView.RowFilter = filterStr;
                filterDesc = memoryStr;
                filter = filterStr;
                
            } else {
                dataSet.Project.DefaultView.RowFilter = string.Empty;
            }
            inSelectionLabel.Text = musikantDataGridView.RowCount.ToString();
             * */
        }

        private void genreComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void modifiedComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void statusComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void potentialComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void groupComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void tempoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void musikantDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            
            if (musikantDataGridView.CurrentRow == null || musikantDataGridView.CurrentRow.DataBoundItem == null) return;
            
            DataRowView obj = (DataRowView)musikantDataGridView.CurrentRow.DataBoundItem;
            curProject = (MusikantDataSet.ProjectRow)obj.Row;
            editDawComboBox.SelectedItem = curProject.DAW;
            editGenreComboBox.SelectedItem = curProject.Genre;
            editStatusComboBox.SelectedIndex = curProject.Status - 1;
            editPotentialComboBox.SelectedIndex = curProject.Potential - 1;
            editTempoUpDown.Value = curProject.Tempo;
            infosTextBox.Text = curProject.Infos;
            ideasTextBox.Text = curProject.Ideas;
            todoTextBox.Text = curProject.Todos;
            editRewireCheckBox.Checked = Boolean.Parse(curProject.Rewire);
            Console.WriteLine("############# " + curProject.Group);
            setGroups(curProject.Group);
            setTempoValues(curProject.Tempo);

            facade.ListDirectory(filesTreeView, curProject);
            fileInfoLabel.Text = "";

            mediaPlayer.Ctlcontrols.stop();
            //FileInfo soundFile = facade.FindFinalOrDemo(curProject.ProjectID);
            //preparePlayer(soundFile);
        }

        private void editDawComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (curProject == null) return;
            curProject.DAW = editDawComboBox.SelectedItem.ToString();
            musikantDataGridView.Invalidate();

        }

        private void MusikantForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            dataSet.WriteXml(xmlFile);

            // Copy window location to app settings
            Settings.Default.WindowLocation = this.Location;
            Settings.Default.Save();
        }

        private void editStatusComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (curProject == null) return;
            curProject.Status = editStatusComboBox.SelectedIndex + 1;
            musikantDataGridView.Invalidate();
        }

        private void editGenreComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (curProject == null) return;
            curProject.Genre = editGenreComboBox.SelectedItem.ToString();
            musikantDataGridView.Invalidate();
        }

        private void editPotentialComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (curProject == null) return;
            curProject.Potential = editPotentialComboBox.SelectedIndex + 1;
            musikantDataGridView.Invalidate();
        }

        private void infosTextBox_TextChanged(object sender, EventArgs e)
        {
            curProject.Infos = infosTextBox.Text;
        }

        private void ideasTextBox_TextChanged(object sender, EventArgs e)
        {
            curProject.Ideas = ideasTextBox.Text;
        }

        private void todoTextBox_TextChanged(object sender, EventArgs e)
        {
            curProject.Todos = todoTextBox.Text;
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            filterTextBox.Text = "";
            dawComboBox.SelectedIndex = -1;
            genreComboBox.SelectedIndex = -1;
            statusComboBox.SelectedIndex = -1;
            potentialComboBox.SelectedIndex = -1;
            modifiedComboBox.SelectedIndex = -1;
            rewireCheckBox.Checked = false;
            groupComboBox.SelectedIndex = -1;
            tempoComboBox.SelectedIndex = -1;
            filterDataGridView();
        }

        

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void filesTreeView_DoubleClick(object sender, EventArgs e)
        {
            if (filesTreeView.SelectedNode.Tag != null)
            {
                Console.WriteLine("Doppelklicke " + filesTreeView.SelectedNode.Tag);
                FileInfo clickedFile = new FileInfo(filesTreeView.SelectedNode.Tag.ToString());
                bool isSound = facade.IsSoundFile(clickedFile);
                if (isSound)
                {
                    playSound(clickedFile.FullName);
                }
                else
                {
                    Process.Start(filesTreeView.SelectedNode.Tag.ToString());
                }
            }
        }

        private void saveMenuItem_Click(object sender, EventArgs e)
        {
            saveDataSet();
        }

        private void filesTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null && e.Node.Tag.GetType() == typeof(string))
            {
                FileInfo file = new FileInfo(e.Node.Tag.ToString());
                fileInfoLabel.Text = StringUtils.Truncate(file.Name)
                    + " (" + file.LastWriteTime.ToString("dd.MM.yy H:mm") + ", " // Datum
                    + StringUtils.FormatFileSize(file.Length) +  ")"; // Filesize
            }
            else if (e.Node != null && e.Node.Tag.GetType() == typeof(DirectoryInfo))
            {
                DirectoryInfo dirInfo = (DirectoryInfo)e.Node.Tag;
                long dirSize = facade.DirectorySize(dirInfo);
                fileInfoLabel.Text = StringUtils.Truncate(dirInfo.Name)
                    + " (" + facade.NumberOfFiles(dirInfo) + " files, "
                    + StringUtils.FormatFileSize(dirSize) + ")";
            }
            else
            {
                fileInfoLabel.Text = "";
            }
        }

        private void rewireCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            filterDataGridView();
        }

        private void editRewireCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            curProject.Rewire = editRewireCheckBox.Checked.ToString();
        }

        private void musikantDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mediaPlayer.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                mediaPlayer.Ctlcontrols.stop();
            }
            else
            {
                DataRowView obj = (DataRowView)musikantDataGridView.CurrentRow.DataBoundItem;
                curProject = (MusikantDataSet.ProjectRow)obj.Row;
                FileInfo demo = facade.FindFinalOrDemo(curProject.ProjectID);
                if (demo != null)
                {
                    mediaPlayer.Ctlenabled = true;
                    mediaPlayer.URL = demo.FullName;
                    //mediaPlayer.Ctlcontrols.stop();
                } 
            }
        }

                
        private void filesTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (filesTreeView.SelectedNode != null && filesTreeView.SelectedNode.Tag.GetType() == typeof(string))
            {
                FileInfo file = new FileInfo(filesTreeView.SelectedNode.Tag.ToString());
                if (e.Label == null || e.Label == "" || e.Label == file.Name) return; // keine Änderung
                try
                {
                    string moveTo = file.Directory.FullName + "\\" + e.Label;
                    file.MoveTo(moveTo);
                    filesTreeView.SelectedNode.Tag = moveTo;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("File couldn't be renamed." + ex.Message, "Error", MessageBoxButtons.OK);
                }
            }
            else if (e.Node != null && e.Node.Tag.GetType() == typeof(DirectoryInfo))
            {
                // Zugriff verweigert, wenn Dir im Explorer offen oder wenn es Unterordner gibt, die im TreeView referenziert sind

                DirectoryInfo dirInfo = (DirectoryInfo)e.Node.Tag;
                if (e.Label == null || e.Label == "" || e.Label == dirInfo.Name) return; // keine Änderung

                foreach (var n in FormUtils.Collect(filesTreeView.Nodes))
                {
                    if (n.Tag.GetType() == typeof(DirectoryInfo)) {
                        DirectoryInfo di = (DirectoryInfo)n.Tag;
                        if (dirInfo != di)
                        {
                            n.Tag = null;
                            di = null;
                        }
                    }
                }
                
                try
                {
                    string moveTo = dirInfo.Parent.FullName + "\\" + e.Label;
                    dirInfo.MoveTo(moveTo);
                }
                catch (Exception ex)
                {
                    MessageBoxEx.Show(this, "Directory couldn't be renamed: " + ex.Message, "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void filesTreeView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (filesTreeView.SelectedNode != null && filesTreeView.SelectedNode.Tag.GetType() == typeof(string))
                {
                    var confirmResult = MessageBoxEx.Show(this, "Are you sure to delete this file?", "Confirm Delete!", MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        FileInfo file = new FileInfo(filesTreeView.SelectedNode.Tag.ToString());
                        try
                        {
                            file.Delete();
                            filesTreeView.Nodes.Remove(filesTreeView.SelectedNode);
                        }
                        catch (Exception ex)
                        {
                            MessageBoxEx.Show(this, "File couldn't be deleted.", "Error", MessageBoxButtons.OK);
                        }
                        
                    }

                }
            }
        }

        private void musikantDataGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            Console.WriteLine(musikantDataGridView.Columns[e.ColumnIndex].Name);
            string fieldName = musikantDataGridView.Columns[e.ColumnIndex].Name;
            if (fieldName == "Progress")
            {
                dataSet.Project.DefaultView.Sort = "Status DESC";
            }
            if (fieldName == "RatingImage")
            {
                dataSet.Project.DefaultView.Sort = "Potential DESC";
            }
        }

        private void playSound(string filePath)
        {
            if (mediaPlayer.playState == WMPLib.WMPPlayState.wmppsPlaying)
            {
                mediaPlayer.Ctlcontrols.stop();
            }
            else
            {
                mediaPlayer.Ctlenabled = true;
                mediaPlayer.URL = filePath;
            }
        }

        private void masteringMenuItem_Click(object sender, EventArgs e)
        {
            if (curProject == null) return;
            string tpl = dataSet.GetConfigValue(Constants.CONFIG_MASTERING_TEMPLATE);
            if (tpl != null)
            {
                DirectoryInfo source = new DirectoryInfo(tpl);
                if (source.Exists)
                {
                    DirectoryInfo current = facade.CurrentDirectory(curProject);
                    foreach (DirectoryInfo sub in current.GetDirectories())
                    {
                        if (sub.Name.ToLower() == Constants.MASTERING.ToLower())
                        {
                            MessageBoxEx.Show(this, "There is already a mastering folder!", "Error", MessageBoxButtons.OK);
                            return;
                        }
                    }
                    DirectoryInfo target = current.CreateSubdirectory(Constants.MASTERING);
                    facade.CopyDirectory(source, target);
                    foreach (FileInfo fileInfo in target.GetFiles())
                    {
                        if (Path.GetFileNameWithoutExtension(fileInfo.Name) == Constants.MASTERING)
                        {
                            string moveTo = fileInfo.Directory.FullName + "\\" + curProject.Name + "." + Constants.MASTERING.ToLower() + fileInfo.Extension;
                            fileInfo.MoveTo(moveTo);
                            //Console.WriteLine("Rename " + curProject.Name + " Mastering" + fileInfo.Extension);
                        }
                    }
                    facade.ListDirectory(filesTreeView, curProject);

                }
                else
                {
                    MessageBoxEx.Show(this, "Mastering template folder does not exist. Edit configuration!", "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void deleteProjectMenuItem_Click(object sender, EventArgs e)
        {
            if (curProject == null) return;
            var confirmResult = MessageBoxEx.Show(this, "Are you sure to delete this project?", "Confirm Delete!", MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                DirectoryInfo current = facade.CurrentDirectory(curProject);
                try
                {
                    Console.WriteLine("Delete " + current.FullName);
                }
                catch (Exception ex)
                {
                    MessageBoxEx.Show(this, "Project couldn't be deleted. " + ex.Message, "Error", MessageBoxButtons.OK);
                }

            }
        }

        private void autosSaveTimer_Tick(object sender, EventArgs e)
        {
            Console.WriteLine("AutoSave Dataset");
            saveDataSet();
        }

        private void saveDataSet()
        {
            dataSet.WriteXml(xmlFile);
        }

        private void editTempoUpDown_ValueChanged(object sender, EventArgs e)
        {
            curProject.Tempo = editTempoUpDown.Value;
            setTempoValues(curProject.Tempo);
        }

        private void groupsListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            string itemText = groupsListBox.Items[e.Index].ToString();
            if (e.NewValue == CheckState.Checked)
            {
                curProject.Group = StringUtils.AddToList(curProject.Group, itemText);
            }
            else
            {
                curProject.Group = StringUtils.RemoveFromList(curProject.Group, itemText);
            }
        }

        private void setGroups(string groups)
        {
            List<string> groupList = StringUtils.SplitAndTrim(groups);
            for (int i = 0; i <= (groupsListBox.Items.Count - 1); i++)
            {
                if (StringUtils.IsInList(groupList, groupsListBox.Items[i].ToString()))
                {
                    groupsListBox.SetItemCheckState(i, CheckState.Checked);
                }
                else
                {
                    groupsListBox.SetItemCheckState(i, CheckState.Unchecked);
                }
           }  
        }

        private void setTempoValues(decimal curTempo)
        {
            tempo4HzTextBox.Text = (curTempo / 60M).ToString("#.##") + " Hz";
            tempo16HzTextBox.Text = (curTempo / 15M).ToString("#.##") + " Hz";
            tempo1MsTextBox.Text = decimal.Truncate((60M / curTempo) * 4000M).ToString() + " ms";
            tempo4MsTextBox.Text = decimal.Truncate((60M / curTempo) * 1000M).ToString() + " ms";
            tempo8MsTextBox.Text = decimal.Truncate((60M / curTempo) * 500M).ToString() + " ms";
            tempo16MsTextBox.Text = decimal.Truncate((60M / curTempo) * 250M).ToString() + " ms";
            tempo4dMsTextBox.Text = decimal.Truncate((60M / curTempo) * 1500M).ToString() + " ms";
        }

        private void preparePlayer(FileInfo soundFile)
        {
            if (soundFile != null)
            {
                mediaPlayer.Ctlenabled = true;
                mediaPlayer.URL = soundFile.FullName;
                mediaPlayer.Ctlcontrols.stop();
            }
            else
            {
                //mediaPlayer.Ctlcontrols.stop();
                mediaPlayer.currentPlaylist.clear();
                mediaPlayer.URL = "d:\\temp\\test.mp3";
                mediaPlayer.Ctlcontrols.stop();
                //mediaPlayer.Ctlenabled = false;
                
            }
        }

        private void btnSet1_Click(object sender, EventArgs e)
        {
            setMemory(1);
            btnMemory1.Text = shortenFilterDesc(this.filterDesc);
        }

        private void btnSet2_Click(object sender, EventArgs e)
        {
            setMemory(2);
            btnMemory2.Text = shortenFilterDesc(this.filterDesc);
        }

        private void btnSet3_Click(object sender, EventArgs e)
        {
            setMemory(3);
            btnMemory3.Text = shortenFilterDesc(this.filterDesc);
        }

        private void btnMemory1_Click(object sender, EventArgs e)
        {
            recall(1);
        }

        private void btnMemory2_Click(object sender, EventArgs e)
        {
            recall(2);
        }

        private void btnMemory3_Click(object sender, EventArgs e)
        {
            recall(3);
        }

        private void btnSet4_Click(object sender, EventArgs e)
        {
            setMemory(4);
            btnMemory4.Text = shortenFilterDesc(this.filterDesc);
        }

        private void btnMemory4_Click(object sender, EventArgs e)
        {
            recall(4);
        }

        private String shortenFilterDesc(string desc)
        {
            if (desc.Length > 28)
            {
                return desc.Substring(0, 26) + "..";
            }
            return desc;
        }

        private void setMemory(int slot)
        {
            dataSet.SetConfigValue("Filter" + slot, filter);
            dataSet.SetConfigValue("FilterDesc" + slot, filterDesc);
        }

        private void recall(int slot)
        {
            string filter = dataSet.GetConfigValue("Filter" + slot);
            if (filter != null)
            {
                string[] parts = filter.Split(',');
                object[] filterParams = {"", -1, -1, -1, -1, -1, -1, -1, false};
                filterParams[0] = parts[0].Trim();
                filterParams[1] = Int32.Parse(parts[1]);
                filterParams[2] = Int32.Parse(parts[2]);
                filterParams[3] = Int32.Parse(parts[3]);
                filterParams[4] = Int32.Parse(parts[4]);
                filterParams[5] = Int32.Parse(parts[5]);
                filterParams[6] = Int32.Parse(parts[6]);
                filterParams[7] = Int32.Parse(parts[7]);
                filterParams[8] = Boolean.Parse(parts[8]);
                filterDataGridView(filterParams);
            }
        }

        private void rescanMenuItem_Click(object sender, EventArgs e)
        {
            facade.Initialize();
        }

        private void renameProjectMenuItem_Click(object sender, EventArgs e)
        {
             
            DataRowView obj = (DataRowView)musikantDataGridView.CurrentRow.DataBoundItem;
            if (obj == null) return;
            curProject = (MusikantDataSet.ProjectRow)obj.Row;
            try
            {
                string promptValue = Prompt.ShowDialog("New project name", "Rename project", StringUtils.RemoveStartingNumber(curProject.Name));
                //mediaPlayer.Ctlcontrols.stop();
                //mediaPlayer.currentPlaylist.clear();
                //mediaPlayer.Ctlenabled = false; // vorhandenes Demo verhindert Folderumbenennung
                //mediaPlayer.close();
                preparePlayer(null);
                filesTreeView.Nodes.Clear();
                facade.RenameProject(curProject, promptValue);
                musikantDataGridView_SelectionChanged(null, null);
            }
            catch (Exception ex)
            {
                MessageBoxEx.Show(this, "Project couldn't be renamed. " + ex.Message, "Error", MessageBoxButtons.OK);
            }
        }

        private void MusikantForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            // STRG + S => Speichern
            if (((Control.ModifierKeys & Keys.Control) == Keys.Control) && e.KeyChar == (char)19)
            {
                Console.WriteLine("Save");
                saveDataSet();
            }
            // STRG + R => Rescan
            if (((Control.ModifierKeys & Keys.Control) == Keys.Control) && e.KeyChar == (char)18)
            {
                Console.WriteLine("Rescan");
                facade.Initialize();
            }
        }

        private void musikantDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        
    }
}
