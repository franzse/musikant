﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Musikant.Forms
{
    public static class Prompt
    {
        public static string ShowDialog(string label, string caption, string defaultValue)
        {
            Form prompt = new Form()
            {
                Width = 500,
                Height = 70,
                FormBorderStyle = FormBorderStyle.FixedDialog,
                Text = caption,
                StartPosition = FormStartPosition.CenterScreen
            };
            Label textLabel = new Label() { Left = 10, Top = 12, Text = label };
            TextBox textBox = new TextBox() { Left = 120, Top = 12, Width = 250 };
            textBox.Text = defaultValue;
            Button confirmation = new Button() { Text = "Ok", Left = 380, Width = 100, Top = 12, DialogResult = DialogResult.OK };
            confirmation.Click += (sender, e) => { prompt.Close(); };
            prompt.Controls.Add(textBox);
            prompt.Controls.Add(confirmation);
            prompt.Controls.Add(textLabel);
            prompt.AcceptButton = confirmation;

            return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
        }
    }
}
