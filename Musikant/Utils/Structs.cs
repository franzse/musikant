﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Musikant.Utils
{
    struct MusikantDirInfo
    {
        public string path;
        public int files;
        public long size;
    }; 
}
