﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Musikant.Data;
using System.Text.RegularExpressions;

namespace Musikant.Utils
{
    class StringUtils
    {
        public static long StartingNumber(string text)
        {
            // erstes Leerzeichen
            string use = text.Trim();
            if (use.IndexOf(' ') == -1)
            {
                return ParseNumber(use);
            }
            else
            {
                use = use.Substring(0, use.IndexOf(' '));
                return ParseNumber(use);
            }
        }

        public static string RemoveStartingNumber(string text)
        {
            return Regex.Replace(text, "^[0-9]+", string.Empty).Trim();
        }

        public static string GetStartingNumber(string text)
        {
            return Regex.Match(text, @"^\d+").ToString();
        }

        public static long ParseNumber(string text) {
            long parsedNumber;
            bool res = long.TryParse(text, out parsedNumber);
            if (res == false)
            {
                return -1;
            }
            else
            {
                return parsedNumber;
            }
        }

        public static bool IsNumber(string text)
        {
            long num1;
            bool res = long.TryParse(text, out num1);
            return res;
        }

        public static bool IsInList(List<string> list, string value)
        {
            return list.Any(value.Contains);
        }

        public static bool IsInDict(Dictionary<string, string> dict, string key)
        {
            return dict.ContainsKey(key);
        }

        public static string FormatFileSize(long length)
        {
            string sLen = length.ToString();
            if (length >= (1 << 30))
                sLen = string.Format("{0} Gb", length >> 30);
            else
                if (length >= (1 << 20))
                    sLen = string.Format("{0} Mb", length >> 20);
                else
                    if (length >= (1 << 10))
                        sLen = string.Format("{0} Kb", length >> 10);
                    else
                        sLen = "0 Kb";
            return sLen;
        }

        public static string Truncate(string value) {
            if (value.Length > Constants.MAX_LENGTH)
            {
                return value.Substring(0, Constants.MAX_LENGTH) + "..";
            }
            return value;
        }

        public static List<string> SplitByLinebreaks(string input)
        {
            string[] lines = input.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
    
            );
            List<string> list = new List<string>();
            foreach(string line in lines) {
                if (!string.IsNullOrEmpty(line.Trim())) list.Add(line.Trim());
            }
            //Array.ForEach(lines, element => if (string.IsNullOrEmpty(element)) list.Add(element.Trim()));
            return list;
        }

        public static string AddToList(string before, string value)
        {
            List<string> List = SplitAndTrim(before);
            if ( ! List.Contains(value)) {
                List.Add(value);
            }
            return String.Join(" | ", List.ToArray());
        }

        public static string RemoveFromList(string before, string value)
        {
            List<string> List = SplitAndTrim(before);
            if (List.Contains(value))
            {
                List.Remove(value);
            }
            return String.Join(" | ", List.ToArray());
        }

        public static List<string> SplitAndTrim(string value)
        {
            string[] groupArr = (String.IsNullOrEmpty(value)) ? new string[0] : value.Split('|');
            List<string> groupList = groupArr.ToList();
            return groupList.Select(s => s.Trim()).ToList(); ;
        }
    }
}
