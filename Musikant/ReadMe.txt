﻿Musikant Project Management Tool

You need to edit these parameters in settings.xml:
- ProjectDirectory

You can edit the parameter MasteringTemplate, if you want to use the feature "Create Mastering subfolder".
You can edit the parameter Groups, if you want to use Groups as a filter.
You can edit the parameter Genres, if the given genres doesn't fit your needs.
