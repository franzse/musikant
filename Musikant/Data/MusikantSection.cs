﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Musikant.Data
{
    class MusikantSection : ConfigurationSection
    {
        [ConfigurationProperty("projectDirectory", DefaultValue = "Arial", IsRequired = true)]
        public String Name
        {
            get
            {
                return (String)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }
    }
}
