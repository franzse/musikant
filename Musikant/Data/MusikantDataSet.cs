﻿using System.Collections.Generic;
using System.Linq;
using Musikant.Utils;

namespace Musikant.Data {
    
    
    public partial class MusikantDataSet {

        public string GetConfigValue(string key)
        {
            if (this.Config.AsEnumerable().Any(row => key == row.Key))
            {
                return this.Config.Where(row => row.Key == key).First().Value;
            }
            else
            {
                return null;
            }
        }

        public void SetConfigValue(string key, string value)
        {
            if (this.Config.AsEnumerable().Any(row => key == row.Key))
            {
                this.Config.Where(row => row.Key == key).First().Value = value;
            }
            else
            {
                ConfigRow newConfig = this.Config.NewConfigRow();
                newConfig.Key = key;
                newConfig.Value = value;
                this.Config.AddConfigRow(newConfig);
            }
        }

        public List<string> GetConfigList(string key, bool addEmptyItem, bool addUnknownItem)
        {
            List<string> resultList = new List<string>();
            if (addEmptyItem) resultList.Add("");
            if (addUnknownItem) resultList.Add(Constants.UNKNOWN);
            resultList.AddRange(StringUtils.SplitByLinebreaks(this.GetConfigValue(key)));
            return resultList;
        }
    }
}
