﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Musikant.Data
{
    class Constants
    {
        public static string TRUE = "True";
        public static string UNKNOWN = "Unknown";
        public static string EXT_REASON = ".reason";
        public static string DAW_REASON = "Reason";

        public static string MASTERING = "Mastering";

        public static int MAX_LENGTH = 50;

        public static string CONFIG_PROJECT_DIR = "ProjectDirectory";
        public static string CONFIG_GROUPS = "Groups"; 
        public static string CONFIG_GENRES = "Genres";
        public static string CONFIG_MASTERING_TEMPLATE = "MasteringTemplate";
    }
}
